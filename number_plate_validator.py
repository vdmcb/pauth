from camera import CameraManipulator
from vision_api import VisionAPI

import cv2
import re


class PlateExtractor(object):
    def __init__(self):
        self.api = VisionAPI()
        self.camera = CameraManipulator()

        self.cascade_path = 'cascade.xml'
        self.classifier = cv2.CascadeClassifier(self.cascade_path)
        self.counties = ['AB', 'AG', 'AR', 'B', 'BC', 'BH', 'BN', 'BR', 'BT', 'BV', 'BZ',
                         'CJ', 'CL', 'CS', 'CT', 'CV', 'DB', 'DJ', 'GJ', 'GL', 'GR', 'HD',
                         'HR', 'IF', 'IL', 'IS', 'MH', 'MM', 'MS', 'NT', 'OT', 'PH', 'SB',
                         'SJ', 'SM', 'SV', 'TL', 'TM', 'TR', 'VL', 'VN', 'VS']
        self.plate_pattern = '[A-Z]{1,2}[0-9]{2,3}[A-Z]{3}'
        self.county_pattern = '^[A-Z]{1,2}'

    def regex_validation(self, plate_number):
        valid_plate_patterns = re.findall(self.plate_pattern, plate_number)
        if len(valid_plate_patterns) > 0:
            valid_plate_pattern = valid_plate_patterns[0]
            plate_county = self.get_plate_county(valid_plate_pattern)
            if len(plate_county) > 0 and plate_county[0] in self.counties:
                return valid_plate_patterns[0]
        return None

    def get_plate_county(self, plate_number):
        return re.findall(self.county_pattern, plate_number)

    # def get_plate_regex_engine(self):
    #     return self.regex_engine

    def detect_using_cascade(self, frame):
        number_plate = self.classifier.detectMultiScale(frame, 1.1, 3, minSize=(20, 20))

        if number_plate is None:
            print('Failed to detect number plate.')

        else:
            for (x, y, w, h) in number_plate:
                r = max(w, h) / 2
                center_x = x + w / 2
                center_y = y + h / 2
                nx = int(center_x - r)
                ny = int(center_y - r)
                nr = int(r * 2)

                return frame[ny:ny + nr, nx:nx + nr]

    def get_plate_number(self, frame, extension):
        possible_number_plates = self.api.get_possible_plate_numbers(self.camera.encode_frame(frame, extension))
        for possible_number_plate in possible_number_plates:
            validated_plate = self.regex_validation(possible_number_plate)
            if validated_plate is not None:
                return validated_plate
        return None
