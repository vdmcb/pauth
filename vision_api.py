from google.cloud import vision

class VisionAPI(object):
    def __init__(self):
        self.vision_client = vision.ImageAnnotatorClient()

    @staticmethod
    def _extract_alphanumeric(input_string):
        from string import ascii_letters, digits
        return "".join([ch for ch in input_string if ch in (ascii_letters + digits)])

    def get_possible_plate_numbers(self, image_as_binary):
        content = image_as_binary.read()
        image = vision.types.Image(content=content)
        response = self.vision_client.text_detection(image=image)
        annotations = response.text_annotations

        possible_plate_numbers = []
        if len(annotations) > 0:
            found_text = annotations[0].description
            individual_strings = found_text.splitlines()
            individual_strings = [self._extract_alphanumeric(element.replace(' ', '').upper()) for element in individual_strings]
            possible_plate_numbers += individual_strings

        return possible_plate_numbers
