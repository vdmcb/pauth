import cv2
import io


class CameraManipulator(object):
    def __init__(self):
        # start the camera stream
        pass

    def retrieve_frame(self):
        img = cv2.imread('1.jpg')
        return img

    @staticmethod
    def encode_frame(frame, extension):
        successful, encoded = cv2.imencode('.' + str(extension), frame)
        binary_image = io.BytesIO(encoded)
        return binary_image
