from number_plate_validator import PlateExtractor

import os
import cv2


def test_directory_demo():

    plate_extractor = PlateExtractor()

    test_images = os.listdir('./test_images')
    for image_path in test_images:
        extension = image_path.split('.')[1]
        image_path = os.path.join('./test_images', image_path)
        frame = cv2.imread(image_path)

        validated_frame_using_cascade = plate_extractor.detect_using_cascade(frame)
        if validated_frame_using_cascade is not None:
            target_frame = validated_frame_using_cascade
        else:
            target_frame = frame

        plate_number = plate_extractor.get_plate_number(target_frame, extension)
        if plate_number is None:
            target_frame = frame
            plate_number = plate_extractor.get_plate_number(target_frame, extension)

        if plate_number is not None:
            print(plate_number)


if __name__ == '__main__':
    test_directory_demo()

